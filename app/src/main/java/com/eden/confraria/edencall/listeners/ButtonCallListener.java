package com.eden.confraria.edencall.listeners;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.EditText;

import com.eden.confraria.edencall.MainActivity;
import com.eden.confraria.edencall.R;
import com.eden.confraria.edencall.model.PhoneCallURIGenerator;

/**
 * Created by Rogerio Libarino Aguilar on 01/03/2015.
 */
public class ButtonCallListener implements View.OnClickListener {
    private MainActivity activity;
    private String phoneNumber;


    public ButtonCallListener(MainActivity mainActivity) {
        this.activity = mainActivity;
    }

    @Override
    public void onClick(View v) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        Uri phoneCallUri = generatePhoneCallUri();
        callIntent.setData(phoneCallUri);
        this.activity.startActivity(callIntent);
    }

    private Uri generatePhoneCallUri() {
        PhoneCallURIGenerator callUriGenerator = new PhoneCallURIGenerator(getPhoneNumber());
        callUriGenerator.setServiceNumber(getServicePhoneNumber());
        return callUriGenerator.generateUriPhoneAndDTMF();
    }

    public String getServicePhoneNumber() {
        EditText inputServicePhone = (EditText) activity.findViewById(R.id.input_service_phone);
        return inputServicePhone.getText().toString();
    }

    public String getPhoneNumber() {
        EditText inputPhoneNumber = (EditText) activity.findViewById(R.id.input_phone_number);
        return inputPhoneNumber.getText().toString();
    }
}