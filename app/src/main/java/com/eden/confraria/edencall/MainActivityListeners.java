package com.eden.confraria.edencall;

import android.widget.Button;

import com.eden.confraria.edencall.listeners.ButtonCallListener;

/**
 * Created by Rogerio Libarino Aguilar on 01/03/2015.
 */
public class MainActivityListeners {

    private final MainActivity activity;
    private final Button buttonCall;

    public MainActivityListeners(MainActivity activity) {
        this.activity = activity;
        buttonCall = (Button) activity.findViewById(R.id.button_call);
    }

    public void setListeners() {
        setListenerToTheButtonCall();
    }

    private void setListenerToTheButtonCall() {
        buttonCall.setOnClickListener(new ButtonCallListener(activity));
    }
}
