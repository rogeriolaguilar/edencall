package com.eden.confraria.edencall.model;

import android.net.Uri;

/**
 * Created by Rogerio Aguilar on 28/02/2015.
 */
public class PhoneCallURIGenerator {
    private String serviceNumber = "988881234";//TODO ver como tratar o numero default
    private String phoneNumber;
    private String delimiter = ",,";

    public PhoneCallURIGenerator() {
        super();
    }

    public PhoneCallURIGenerator(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Uri generateUriPhoneAndDTMF() {
        return Uri.parse("tel:".concat(serviceNumber).concat(delimiter).concat(phoneNumber));
    }

    public void setServiceNumber(String serviceNumber) {
        if (serviceNumber != null && !serviceNumber.isEmpty())
            this.serviceNumber = serviceNumber;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }
}
